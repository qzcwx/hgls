#include <stdlib.h>
#include "block.h"

int
main(int argc, char *argv[]) {
    GArray *inst;
    guint n = 0;
    guint m = 0;
    // n_old and m_old are required for freeing memory properly
    guint n_old;
    guint m_old;

    guint *k;
    GArray **negf = NULL, **mask = NULL;
    GArray **lSub = NULL; /* the inverted list of subfunctions that has a particular variable */

    // set default values for command line options
    static gchar *inst_name = NULL;
    static gint threshold = 6;
    static gchar *dnf_outfile = NULL;
    static gboolean hybrid_rep = FALSE; // hybrid whether to use hybrid representation or not

    static GOptionEntry entries[] =
            {
                    {"inst",        'i'/*short-name*/, 0/*flags*/, G_OPTION_ARG_STRING/*arg*/, &inst_name,
                            "Specify the path of instance file in CNF format",                 "path of instance file"},
                    {"threshold",   't',               0,          G_OPTION_ARG_INT,           &threshold,
                            "threshold on the number of variables in one block: default to 6", "threshold"},
                    {"dnf_outfile", 'd',               0,          G_OPTION_ARG_STRING,        &dnf_outfile,
                            "The filename of the generated dnf isntance",                      "dnf output filename"},
                    {"hybrid_rep",  'b',               0,          G_OPTION_ARG_NONE,          &hybrid_rep,
                            "Use hybrid representation of both CNF and DNF", NULL},
                    {NULL}
            };

    GError *error = NULL;
    GOptionContext *context = NULL;
    context = g_option_context_new("CNF to DNF converter");
    g_option_context_add_main_entries(context, entries, NULL);


    if (!g_option_context_parse(context, &argc, &argv, &error)) {
        g_print("option parsing failed: %s\n", error->message);
        exit(1);
    }

    readInstance(&inst, &n, &m, inst_name);
    parseInst(inst, &k, &mask, &negf, n, m, &lSub);
    m_old = m;
    n_old = n;

    GTimer *t = g_timer_new();
    /* assumes that instances are already sorted */
    GArray *clause_block, *var_block;
    GArray *dnf;
    GArray *dnf_clause_num;    /* number of dnf clauses associated with a block of variables */
    if (!hybrid_rep) {
        break_long_clauses(&inst, &n, &m, threshold);
        cluster_clause_inverted_list(threshold, inst, n, &clause_block, &var_block);
        enumerate_solution_subfunction(inst, n, clause_block, var_block, &dnf, &dnf_clause_num);
        g_timer_stop(t);
        /* export_compact_dnf(dnf, dnf_clause_num, n, dnf_outfile); */
        export_compact_dnf_repetition(dnf, dnf_clause_num, n, g_timer_elapsed(t, NULL), threshold, dnf_outfile);
    } else {
        // hybrid representation: keep single CNF clauses as they are
        GArray *cnf_clauses;
        cluster_clause_inverted_list_hybrid(threshold, inst, n, &clause_block, &var_block, &cnf_clauses);
        enumerate_solution_subfunction(inst, n, clause_block, var_block, &dnf, &dnf_clause_num);
        export_compact_dnf_repetition(dnf, dnf_clause_num, n, g_timer_elapsed(t, NULL), threshold, dnf_outfile);
        export_cnf_clauses(inst, cnf_clauses, dnf_outfile);
        g_array_free(cnf_clauses, TRUE);
    }

    g_array_free(dnf_clause_num, TRUE);
    clear_block(dnf);
    clear_block(clause_block);
    clear_block(var_block);

    g_timer_destroy(t);
    clearInst(&inst, &k, &mask, &negf, &lSub, n_old, m_old);
    g_option_context_free(context);

    return 0;
}
