#ifndef _BLOCK_H
#define _BLOCK_H

#include <glib.h>
#include <stdlib.h>
#include <stdio.h>
#include "file.h"

void
        break_long_clauses(GArray **inst, guint *n, guint *m, guint threshold);

void
        cluster_clause_inverted_list(guint thres, GArray *inst, guint n, GArray **clause_block, GArray **var_block);

void
        cluster_clause_inverted_list_hybrid(guint thres, GArray *inst, guint n, GArray **clause_block,
                                            GArray **var_block,
                                            GArray **cnf_clauses);

gdouble *
        enumerate_solution_subfunction(GArray *inst, guint n, GArray *clause_block, GArray *var_block, GArray **dnf,
                                       GArray **dnf_clause_num);

void
        enumeration_by_elimination(GArray *inst, guint n, GArray *clause_block, GArray *var_block,
                                   GArray **grouped_big_ints, GArray **grouped_block_sizes,
                                   GArray **repetitions, GArray **grouped_num_dnfs);

uint64_t
        all_ones(guint block_size);

void
        clear_block(GArray *block);

void
        array_intersection(GArray *target, GArray *source);

gboolean
        subset_of_prev_block(const GArray *clause, GArray *const *inv_list, GArray *inter);

void
        print_inv_list(GArray **inv_list, guint n);

void
        print_array(GArray *arr);

void
        print_block(GArray *clause_block);


#endif
