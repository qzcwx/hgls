/* Divide CNF into blocks of DNF */

#include "block.h"

/* Break long clauses into blocks that are of length equal to threshold, except for the last one */
void
break_long_clauses(GArray **inst, guint *n, guint *m, guint threshold) {
    GArray *clause;
    guint i = 0;
    guint j;
    gint neg;
    while (i < *m) {
        clause = g_array_index(*inst, GArray *, i);
//        printf("i=%d\tm=%d\tlen=%d\tthres=%d\n", i, *m, clause->len, threshold);
        if (clause->len > threshold) {
//            printf("exceed length = %d\n", clause->len);
            guint processed_var = 0;
            guint broken_clause_count = 0;
            while (TRUE) {
                GArray *broken_clause = g_array_new(FALSE, FALSE, sizeof(gint));
                if (threshold + processed_var < clause->len) { // still variables remained
                    for (j = 0; j < threshold - 1; j++) {
                        g_array_append_val(broken_clause, g_array_index(clause, gint, j + processed_var));
                    }

                    /* Add auxiliary variable */
                    (*n)++;
                    neg = -(*n);
                    g_array_append_val(broken_clause, neg);
                    g_array_append_val(clause, (*n));

                    processed_var += j;
//                printf("insert clause at %d\n", i + 1 + broken_clause_count);
                    g_array_insert_val(*inst, i + 1 + broken_clause_count, broken_clause);
//                    printf("processed_var = %d, clause.len = %d\n", processed_var, clause->len);
                    broken_clause_count++;
//                printf("processed_var = %d, j = %d\n", processed_var, j);
                } else if (threshold + processed_var >= clause->len) {
                    // the rest of clause can be covered within threshold, no risk of running over threshold
                    for (j = processed_var; j < clause->len; j++) {
                        g_array_append_val(broken_clause, g_array_index(clause, gint, j));
                    }
                    g_array_insert_val(*inst, i + 1 + broken_clause_count, broken_clause);
                    broken_clause_count++;
                    break;
                }
//                printInst(*inst);
//                printf("\n");
            }
            g_array_free(g_array_index(*inst, GArray *, i), TRUE);
            g_array_remove_index(*inst, i);
//            printf("Instance after removing old clause\n");
//            printInst(*inst);
//            printf("\n");
            i += broken_clause_count;
            *m += broken_clause_count - 1;
        } else {
            i++;
        }
    }
//    printInst(*inst);
//    printf("\n");
}

/* Cluster clauses into blocks according to inverted list of variables
 *
 * thres: threshold for number of variables in each block. thres <= 6, otherwise the pattern can be fitted into BigInt
 * clause_block: clause idx included in each block
 * var_block: var idx included in each block
 * */
void
cluster_clause_inverted_list(guint thres, GArray *inst, guint n, GArray **clause_block, GArray **var_block) {
    (*clause_block) = g_array_new(FALSE, FALSE, sizeof(GArray *));
    (*var_block) = g_array_new(FALSE, FALSE, sizeof(GArray *));

    guint cid;
    guint count = 1;                /* there is at least one block */
    GArray *clause;
    GArray *cur_pool = g_array_new(FALSE, FALSE, sizeof(guint));/* variable pool for current clause*/
    GArray *pre_pool = g_array_new(FALSE, FALSE, sizeof(guint));/* variable pool from previous clauses*/
    GArray **inv_list = g_new(GArray *, n); /* pointer */
    GArray *inter = g_array_new(FALSE, FALSE, sizeof(guint));
//    printf("inst len = %d\n", inst->len);

    /* initialize inverted lists for every variable */
    for (guint i = 0; i < n; i++) {
        inv_list[i] = g_array_new(FALSE, FALSE, sizeof(guint));
    }

    for (cid = 0; cid < inst->len; cid++) {
        /* for each clause */
        clause = g_array_index(inst, GArray *, cid);

        gboolean full = FALSE;
        g_array_set_size(cur_pool, 0);
        g_array_set_size(inter, 0);
//        printf("\nCLAUSE: %d, len=%d\n", cid, clause->len);

        /* printf("\ninter len = %d\n", inter->len); */
        if (inter->len > 0 && subset_of_prev_block(clause, inv_list, inter)) {
            /* intersection is nonempty, insert clauses into (*clause_block) */
            guint block_idx = g_array_index(inter, guint, 0) - 1;
            /* printf("\nblock_idx = %d\n", block_idx); */
            GArray *clause_pool_found = g_array_index((*clause_block), GArray *, block_idx);
            /* if (clause_pool_found){ */
            /* 	printf("clause_pool_found = %d\n", clause_pool_found->len); */
            /* } else { */
            /* 	printf("clause_pool_found is null. count = %d, clause_block len = %d\n", count, clause_block->len); */
            /* } */
            g_array_append_val(
                    clause_pool_found,
                    cid);
            /* printf("g_array_append_val\n"); */

            continue;                    /* skip the rest of code */
        }


        /* there is no previous block containing all variable of current clause */
        for (guint i = 0; i < clause->len; i++) {
            gint term = abs(g_array_index(clause, gint, i));

//            printf("var with sign: %d\t term: %d\n", g_array_index(clause, gint, i), term);
//            printf("pre_pool\n");
//            print_array(pre_pool);

            /* check if term is already in the varPool */
            gboolean in_pool = FALSE;
            for (guint j = 0; j < pre_pool->len; j++) {
                if (abs(term) == abs(g_array_index(pre_pool, guint, j))) {
                    in_pool = TRUE;
                    break;
                }
            }

            /* printf("in_pool = %d, pre_pool.len = %d, cur_pool.len = %d\n", in_pool, pre_pool->len, cur_pool->len); */
            if (!in_pool) {
                if (pre_pool->len + cur_pool->len < thres ||
                    (pre_pool->len + cur_pool->len == thres - 1 && i == clause->len - 1)) {
                    /* plenty of space left or reaching the limit exactly */
                    g_array_append_val(cur_pool, term);
                    /* printf("term %d added to cur_pool->len = %d\n", term, cur_pool->len); */
                } else {
                    /* if the pool is full and there are still more new variables, start a new block */
//                    printf("full\n");

                    full = TRUE;

                    /* copy clause to pre_pool for a fresh start */
                    if (pre_pool->len > 0) {
//                        printf("start a new block\n");
                        g_array_append_val((*var_block), pre_pool);
                        pre_pool = g_array_new(FALSE, FALSE, sizeof(guint));
                        count++;
                    }

                    for (guint j = 0; j < clause->len; j++) {
                        guint var = (guint) abs(g_array_index(clause, gint, j));
                        g_array_append_val(pre_pool, var);
                    }


                    for (guint j = 0; j < clause->len; j++) {
                        term = abs(g_array_index(clause, gint, j));
                        /* printf("count = %d, term = %d\n", count, term); */
                        /* if (inv_list[term-1]->len !=0){ */
                        /*   printf("last elem in inv_list = %d, count = %d\n", g_array_index(inv_list[term-1], guint, (inv_list[term-1])->len - 1 ), count); */
                        /* }else{ */
                        /*   printf("inv_list empty\n"); */
                        /* } */
                        if (inv_list[term - 1]->len == 0 ||
                            g_array_index(inv_list[term - 1], guint, (inv_list[term - 1])->len - 1) != count) {
                            g_array_append_val(inv_list[term - 1], count);
                        }
                    }

                    /* print inv_list */
                    /* print_inv_list(inv_list, n); */
                    g_array_set_size(cur_pool, 0);

                    GArray *last_block = g_array_new(FALSE, FALSE, sizeof(guint));
                    g_array_append_val((*clause_block), last_block);
                    g_array_append_val(last_block, cid);

                    break;
                }
            }
        }

        if (!full) {
            /* successfully add a clause to an existing block */
            /* record the block id of all variables in this clause */
            /* printf("not full, move cur_pool to pre_pool\n"); */

            g_array_append_vals(pre_pool, cur_pool->data, cur_pool->len);
            GArray *last_block;
            if ((*clause_block)->len) {
                last_block = g_array_index((*clause_block), GArray *, (*clause_block)->len - 1);

            } else {
                last_block = g_array_new(FALSE, FALSE, sizeof(guint));
                g_array_append_val((*clause_block), last_block);
            }
            g_array_append_val(last_block, cid);


            for (guint j = 0; j < cur_pool->len; j++) {
                guint term = g_array_index(cur_pool, guint, j);
                /* only add to the inverted list if it is not already there */
                if (inv_list[term - 1]->len == 0 ||
                    g_array_index(inv_list[term - 1], guint, inv_list[term - 1]->len - 1) != count) {
                    g_array_append_val(inv_list[term - 1], count);
                }
            }
            /* print_inv_list(inv_list, n); */
        }
    }

    /* handles the last block */
    /* g_array_append_val(clause_block, clause_pool); */
    g_array_append_val((*var_block), pre_pool);

    /* free memory */
    g_array_free(cur_pool, TRUE);
    /* g_array_free(pre_pool, TRUE); */
    g_array_free(inter, TRUE);
    for (guint i = 0; i < n; i++) {
        g_array_free(inv_list[i], TRUE);
    }
    g_free(inv_list);

//    printf("count = %d, len clause_block = %d\n", count, (*clause_block)->len);
//    printf("var_block\n");
//    print_block(*var_block);
//    printf("\n");
//
//    printf("clause_block\n");
//    print_block(*clause_block);
//    printf("\n");
}


/*
 * Cluster clauses into blocks according to inverted list of variables.
 * Using hybrid representation
 *
 * thres:           threshold for number of variables in each block. thres <= 6, otherwise the pattern can be fitted into BigInt
 * clause_block:    clause idx included in each block
 * var_block:       var idx included in each block
 * cnf_clause:      remaining CNF clauses ids
 * */
void
cluster_clause_inverted_list_hybrid(guint thres, GArray *inst, guint n, GArray **clause_block, GArray **var_block,
                                    GArray **cnf_clauses) {
    (*clause_block) = g_array_new(FALSE, FALSE, sizeof(GArray *));
    (*var_block) = g_array_new(FALSE, FALSE, sizeof(GArray *));
    *cnf_clauses = g_array_new(FALSE, FALSE, sizeof(guint));

    guint cid;
    guint num_block = 1;                /* there is at least one block */
    GArray *clause;
    GArray *cur_pool = g_array_new(FALSE, FALSE, sizeof(guint));/* variable pool for current clause*/
    GArray *pre_pool = g_array_new(FALSE, FALSE, sizeof(guint));/* variable pool from previous clauses*/
    GArray **inv_list = g_new(GArray *, n); /* pointer */
    GArray *inter = g_array_new(FALSE, FALSE, sizeof(guint));
//    printf("inst len = %d\n", inst->len);

    /* initialize inverted lists for every variable */
    for (guint i = 0; i < n; i++) {
        inv_list[i] = g_array_new(FALSE, FALSE, sizeof(guint));
    }

    for (cid = 0; cid < inst->len; cid++) {
        /* for each clause */
        clause = g_array_index(inst, GArray *, cid);

        gboolean full = FALSE;
        g_array_set_size(cur_pool, 0);
        g_array_set_size(inter, 0);
//        printf("\nCLAUSE: %d, len=%d\n", cid, clause->len);

        if (clause->len > thres) {
            /* special handling of long clauses */
            g_array_append_val(*cnf_clauses, cid);
            continue;
        }

        /* printf("\ninter len = %d\n", inter->len); */
        if (inter->len > 0 && subset_of_prev_block(clause, inv_list, inter)) {
            /* intersection is nonempty, insert clauses into (*clause_block) */
            guint block_idx = g_array_index(inter, guint, 0) - 1;
            GArray *clause_pool_found = g_array_index((*clause_block), GArray *, block_idx);
            /* if (clause_pool_found){ */
            /* 	printf("clause_pool_found = %d\n", clause_pool_found->len); */
            /* } else { */
            /* 	printf("clause_pool_found is null. num_block = %d, clause_block len = %d\n", num_block, clause_block->len); */
            /* } */
            g_array_append_val(
                    clause_pool_found,
                    cid);
            continue;                    /* skip the rest of code */
        }


        /* there is no previous block containing all variable of current clause */
        for (guint i = 0; i < clause->len; i++) {
            gint term = abs(g_array_index(clause, gint, i));

//            printf("var with sign: %d\t term: %d\n", g_array_index(clause, gint, i), term);
//            printf("pre_pool\n");
//            print_array(pre_pool);

            /* check if term is already in the varPool */
            gboolean in_pool = FALSE;
            for (guint j = 0; j < pre_pool->len; j++) {
                if (abs(term) == abs(g_array_index(pre_pool, guint, j))) {
                    in_pool = TRUE;
                    break;
                }
            }

            /* printf("in_pool = %d, pre_pool.len = %d, cur_pool.len = %d\n", in_pool, pre_pool->len, cur_pool->len); */
            if (!in_pool) {
                if (pre_pool->len + cur_pool->len < thres ||
                    (pre_pool->len + cur_pool->len == thres - 1 && i == clause->len - 1)) {
                    /* plenty of space left or reaching the limit exactly */
                    g_array_append_val(cur_pool, term);
                    /* printf("term %d added to cur_pool->len = %d\n", term, cur_pool->len); */
                } else {
                    /* if the pool is full and there are still more new variables, start a new block */
//                    printf("full\n");

                    full = TRUE;

                    /* copy clause to pre_pool for a fresh start */
                    if (pre_pool->len > 0) {
//                        printf("start a new block\n");
                        g_array_append_val((*var_block), pre_pool);
                        pre_pool = g_array_new(FALSE, FALSE, sizeof(guint));
                        num_block++;
                    }

                    // add all variable to pre_pool
                    for (guint j = 0; j < clause->len; j++) {
                        guint var = (guint) abs(g_array_index(clause, gint, j));
                        g_array_append_val(pre_pool, var);
                    }

                    for (guint j = 0; j < clause->len; j++) {
                        term = abs(g_array_index(clause, gint, j));
                        /* printf("num_block = %d, term = %d\n", num_block, term); */
                        /* if (inv_list[term-1]->len !=0){ */
                        /*   printf("last elem in inv_list = %d, num_block = %d\n", g_array_index(inv_list[term-1], guint, (inv_list[term-1])->len - 1 ), num_block); */
                        /* }else{ */
                        /*   printf("inv_list empty\n"); */
                        /* } */
                        if (inv_list[term - 1]->len == 0 ||
                            g_array_index(inv_list[term - 1], guint, (inv_list[term - 1])->len - 1) != num_block) {
                            g_array_append_val(inv_list[term - 1], num_block);
                        }
                    }

                    /* print inv_list */
                    /* print_inv_list(inv_list, n); */
                    g_array_set_size(cur_pool, 0);

                    GArray *last_block = g_array_new(FALSE, FALSE, sizeof(guint));
                    g_array_append_val((*clause_block), last_block);
                    g_array_append_val(last_block, cid);

                    break; // no need to continue on the current CNF clause
                }
            }
        }

        if (!full) {
            /* successfully add a clause to an existing block */
            /* record the block id of all variables in this clause */
            /* printf("not full, move cur_pool to pre_pool\n"); */
            g_array_append_vals(pre_pool, cur_pool->data, cur_pool->len);
            GArray *last_block;
            if ((*clause_block)->len) {
                last_block = g_array_index((*clause_block), GArray *, (*clause_block)->len - 1);

            } else {
                last_block = g_array_new(FALSE, FALSE, sizeof(guint));
                g_array_append_val((*clause_block), last_block);
            }
            g_array_append_val(last_block, cid);


            for (guint j = 0; j < cur_pool->len; j++) {
                guint term = g_array_index(cur_pool, guint, j);
                /* only add to the inverted list if it is not already there */
                if (inv_list[term - 1]->len == 0 ||
                    g_array_index(inv_list[term - 1], guint, inv_list[term - 1]->len - 1) != num_block) {
                    g_array_append_val(inv_list[term - 1], num_block);
                }
            }
            /* print_inv_list(inv_list, n); */
        }
    }

    /* handles the last block */
    /* g_array_append_val(clause_block, clause_pool); */
    g_array_append_val((*var_block), pre_pool);

    /* free memory */
    g_array_free(cur_pool, TRUE);
    /* g_array_free(pre_pool, TRUE); */
    g_array_free(inter, TRUE);
    for (guint i = 0; i < n; i++) {
        g_array_free(inv_list[i], TRUE);
    }
    g_free(inv_list);

//    printf("num_block = %d, len clause_block = %d\n", num_block, (*clause_block)->len);
//    printf("var_block\n");
//    print_block(*var_block);
//    printf("\n");
//
//    printf("clause_block\n");
//    print_block(*clause_block);
//    printf("\n");
}


/*
 * check if the current clause is a subset of previous block
 * by intersecting the previous clauses containing each variable.
 * */
gboolean subset_of_prev_block(const GArray *clause, GArray *const *inv_list, GArray *inter) {
    gboolean prev_exist = TRUE;
    for (guint var_idx = 0; var_idx < clause->len; var_idx++) {
        /* for every variables in that clause */
        gint var = g_array_index(clause, gint, var_idx);
        /* printf("%d\t", var); */

        /* lookup the associated clauses in its inverted list */
        GArray *list = inv_list[abs(var) - 1];
        /* printf("inv list len = %d\n", list->len); */
        /* print_array(list); */

        if (list->len == 0) {        /* inverted list is empty  */
            prev_exist = FALSE;
            break;
        }

        // compute the intersection of clauses containing each variable, suggested by inverted list
        /* printf ("inter len before = %d\n", inter->len); */
        /* print_array(inter); */
        if (var_idx == 0) {
            /* printf("inter init len = %d\n", inter->len); */
            g_array_append_vals(inter, list->data, list->len);
        }
        else {
            array_intersection(inter, list);
        }
        /* printf ("inter len after = %d\n", inter->len); */
        /* print_array(inter); */
        /* printf ("\n"); */
        /* if (inter->len){ */
        /* 	printf ("inter[0] = %d\n", g_array_index(inter, guint, 0)); */
        /* } */
    }
    return prev_exist;
}

/* Enumerate solution for all blocks to generate DNF
 *
 * Input:
 *  inst, original instance in CNF
 *  n, number of variables
 *  clause_block, grouping of clauses
 *  var_block, grouping of variables
 * Output:
 *  dnf, dnf clauses
 *  dnf, the number of dnf clauses in each group
 *
 * */
gdouble *
enumerate_solution_subfunction(GArray *inst, guint n, GArray *clause_block, GArray *var_block, GArray **dnf,
                               GArray **dnf_clause_num) {

    guint cid;
    GArray *clauses;
    guint eval;
    gboolean *assignment;
    gint assign_val;
    guint pos;
    GArray *clause;
    GArray *vars;
    gboolean SAT;
    gint lit;
//    guint *pos_vote, *neg_vote;
    gdouble *p = NULL;
    GArray *line;
    guint num_sol;

//    pos_vote = g_new(guint, n);
//    neg_vote = g_new(guint, n);
//    p = g_new(gdouble, n);
    (*dnf) = g_array_new(FALSE, FALSE, sizeof(GArray *));
    (*dnf_clause_num) = g_array_new(FALSE, FALSE, sizeof(guint));

//    for (guint i = 0; i < n; i++) {
//        pos_vote[i] = 0;
//        neg_vote[i] = 0;
//    }

//    printf("clause_block.len = %d, var_block.len = %d\n", clause_block->len, var_block->len);
    for (guint block_idx = 0; block_idx < clause_block->len; block_idx++) {
        clauses = g_array_index(clause_block, GArray *, block_idx);
        vars = g_array_index(var_block, GArray *, block_idx);
//        printf("block_idx = %d\n", block_idx);

        /* evaluate all 2^{block.len} assignments associated with subfunctions  */

        /* initialize all to zeros */
        assignment = g_new(gboolean, vars->len);
        for (guint i = 0; i < vars->len; i++) {
            assignment[i] = FALSE;
        }

        /* printf("print vars\n"); */
        /* print_array(vars); */
        /* printf("\n"); */
        num_sol = 0;

        while (TRUE) {
            /* collect all clauses from one block to evaluate the current clustered subfunctions */

            /* printf("current assignment...\n"); */
            /* for (guint k=0; k<vars->len; k++){ */
            /* 	if (assignment[k]){ */
            /* 	  printf("%d ", g_array_index(vars, gint, k)); */
            /* 	}else{ */
            /* 	  printf("-%d ", g_array_index(vars, gint, k)); */
            /* 	} */
            /* } */
            /* printf("\n"); */

            eval = 0;
            for (guint i = 0; i < clauses->len; i++) { // evaluate all clauses in the block under current assignment
                cid = g_array_index(clauses, guint, i);
                clause = g_array_index(inst, GArray *, cid);
                /* printf ("cid = %d\n",cid); */
                SAT = FALSE;
                for (guint j = 0; j < clause->len; j++) {
                    lit = g_array_index(clause, gint, j);
                    /* printf ("lit = %d\n", lit); */
                    assign_val = -1;

                    /* find its assignment among vars from one block of vars_block */
                    for (guint k = 0; k < vars->len; k++) {
                        if (g_array_index(vars, gint, k) == abs(lit)) {
                            assign_val = assignment[k];
                            break;
                        }
                    }

                    if (assign_val == -1) { /* find its matching  */
                        printf("lit %d not found\n", lit);
                    }
                    /* printf("lit = %d, assign_val = %d\n", lit, assign_val); */
                    if ((lit < 0 && !assign_val) || (lit > 0 && assign_val)) {
                        /* printf("** current clause SAT\n"); */
                        SAT = TRUE;
                        break;
                    }
                }
                eval += SAT;
            }

//            printf("eval = %d/%d\n", eval, clauses->len);

            if (eval == clauses->len) {
                /* printf("* current subfunction is SAT\n"); */
                line = g_array_new(FALSE, FALSE, sizeof(gint));
                for (guint k = 0; k < vars->len; k++) {
                    /* if (assignment[k]){ */
                    /* 	printf("%d ", abs(g_array_index(vars, gint, k))); */
                    /* }else{ */
                    /* 	printf("-%d ", abs(g_array_index(vars, gint, k))); */
                    /* } */
                    lit = assignment[k] ? g_array_index(vars, gint, k) : -g_array_index(vars, gint, k);
                    g_array_append_val(line, lit);
                }
                /* g_array_sort (line, (GCompareFunc)absComparator);  */
                g_array_append_val((*dnf), line);
                ++num_sol;
//                for (guint i = 0; i < vars->len; i++) {
//                    if (assignment[i]) {
//                        pos_vote[abs(g_array_index(vars, gint, i)) - 1]++;
//                    } else {
//                        neg_vote[abs(g_array_index(vars, gint, i)) - 1]++;
//                    }
//                }
            }

            pos = 0;
            /* move to the next assignment */
            while (pos < vars->len && assignment[pos]) {
                assignment[pos++] = FALSE;
            }

            if (pos < vars->len) {
                assignment[pos] = TRUE;
            } else {
                break;
            }
        }
        if (!num_sol) {
            printf("UNSAT");
            exit(2);
        }
        g_array_append_val((*dnf_clause_num), num_sol);
        g_free(assignment);
    }

    /* printf ("pos_vote\n"); */
    /* for (guint i=0; i<n; i++){ */
    /* 	printf ("%d ",pos_vote[i]); */
    /* } */
    /* printf ("\n"); */

    /* printf ("neg_vote\n"); */
    /* for (guint i=0; i<n; i++){ */
    /* 	printf ("%d ",neg_vote[i]); */
    /* } */
    /* printf ("\n"); */

//    guint pos_count = 0, neg_count = 0, equal_count = 0;
//
//    for (guint i = 0; i < n; i++) {
//        /* compute pos_rate (p) */
//        p[i] = pos_vote[i] / (gdouble) (pos_vote[i] + neg_vote[i]);
//        if (p[i] < 0.5) {
//            neg_count++;
//        } else if (p[i] == 0.5) {
//            equal_count++;
//        } else {
//            pos_count++;
//        }
//        /* printf ("%.2f ", p[i]); */
//    }
//    /* printf ("\n"); */
//
//    /* for (guint i=0; i<n; i++){ */
//    /* 	printf ("%d/%d/%.2f\n", pos_vote[i], neg_vote[i], p[i]);  */
//    /* } */
//
//    /* printf("pos/neg/equal = %d/%d/%d\n", pos_count, neg_count, equal_count); */
//    g_free(pos_vote);
//    g_free(neg_vote);

    return p;
}

/* Enumeration DNF solutions by eliminating forbidden solutions by CNF clauses
 *
 * Input:
 *  inst,           original instance in CNF
 *  n,              number of variables
 *  clause_block,   grouping of clauses
 *  var_block,      grouping of variables
 *
 * Output:
 *  grouped_big_ints,       DNF functions grouped by (big_ints, block_size)
 *  grouped_block_sizes,    DNF functions grouped by (big_ints, block_size)
 *  repetitions,            how many times the same DNF function patterns are repeated
 *  grouped_num_dnfs,       Number of DNF solutions for the DNF function patterns, for sanity check
 * */
void enumeration_by_elimination(GArray *inst, guint n, GArray *clause_block, GArray *var_block,
                                GArray **grouped_big_ints, GArray **grouped_block_sizes,
                                GArray **repetitions, GArray **grouped_num_dnfs) {
    guint cid;
    GArray *clauses;
    GArray *clause;
    GArray *vars;
    uint64_t big_int;
    gint lit;
    GArray *indices;
    guint idx;

    (*grouped_big_ints) = g_array_new(FALSE, FALSE, sizeof(uint64_t));
    (*grouped_block_sizes) = g_array_new(FALSE, FALSE, sizeof(guint));
    (*repetitions) = g_array_new(FALSE, FALSE, sizeof(guint));
    (*grouped_num_dnfs) = g_array_new(FALSE, FALSE, sizeof(guint));
    indices = g_array_new(FALSE, FALSE, sizeof(guint));


    // start with a full binary for assuming all partial assignments lead to partial solution
    for (guint block_idx = 0; block_idx < clause_block->len; block_idx++) {

        big_int = all_ones(clause_block->len); // 18446744073709551615 (2^64-1)

        clauses = g_array_index(clause_block, GArray*, block_idx);
        vars = g_array_index(var_block, GArray*, block_idx);
        printf("print vars\n");
        print_array(vars);

        for (guint i = 0; i < clauses->len; i++) {
            cid = g_array_index(clauses, guint, i);
            clause = g_array_index(inst, GArray*, cid);

            /* iterate through one block of vars_block to find index for current literal */
            printf("indices\n");
            for (guint j = 0; j < clause->len; j++) {
                lit = g_array_index(clause, gint, j);
                printf("lit = %d\n", lit);
                //todo: use a map associate variables to their indices in block
                for (guint k = 0; k < vars->len; k++) {
                    if (g_array_index(vars, gint, k) == abs(lit)) {
                        printf("%d", k);
                        g_array_append_val(indices, k);
                        break;
                    }
                }
                printf("\n");
            }

            /* eliminate solutions forbidden by clause in big_int */
            for (guint j = 0; j < clause->len; j++) {
                lit = g_array_index(clause, gint, j);
                idx = g_array_index(indices, guint, j);
                /* fill the rest of position */
            }
        }
    }

}

/* get all ones solution given block_size */
uint64_t all_ones(guint block_size) {
    if (block_size == 6) return 0xffffffffffffffffU; // return hardcoded max_int64 to avoid overflow
    guint num_sols = 1 << block_size;
    return (((uint64_t) 1) << num_sols) - 1;
}

void
clear_block(GArray *blocks) {
    for (guint i = 0; i < blocks->len; i++) {
        GArray *block = g_array_index(blocks, GArray *, i);
        g_array_free(block, TRUE);
    }
    g_array_free(blocks, TRUE);
}

void
array_intersection(GArray *target, GArray *source) {
    /* array intersection of two lists target and source, save the output in source */

    /* printf ("array inter\n"); */
    gboolean inter;
    guint var;
    /* printf("target.len = %d, source.len = %d\n", target->len, source->len); */
    for (int i = 0; i < target->len; i++) {
        var = g_array_index(target, guint, i);
        /* printf ("target var = %d\n", var); */
        inter = FALSE;
        for (int j = 0; j < source->len; j++) {
            /* printf ("source var = %d\n", g_array_index(source, guint, j)); */
            if (var == g_array_index(source, guint, j)) {
                inter = TRUE;
                /* printf("inter found\n"); */
                break;
            }
        }
        if (!inter) {
            /* there is no such element in the source, it must be removed */
            /* printf("target.len before = %d\n", target->len); */
            g_array_remove_index_fast(target, i);
            /* printf("target.len after = %d\n", target->len); */
            i--;
        }
    }
}


void
print_inv_list(GArray **inv_list, guint n) {
    printf("inv_list\n");
    for (int i = 0; i < n; i++) {
        if (inv_list[i]->len) {
            printf("%d (len: %d) -> ", i + 1, inv_list[i]->len);
            GArray *one_list = inv_list[i];
            for (int j = 0; j < one_list->len; j++) {
                printf("%d\t", g_array_index(one_list, guint, j));
            }
            printf("\n");
        }
    }
}

void
print_array(GArray *arr) {
    printf("len = %d\n", arr->len);
    for (guint i = 0; i < arr->len; i++) {
        printf("%d\t", g_array_index(arr, gint, i));
    }
    printf("\n");
}

void
print_block(GArray *clause_block) {
    printf("len = %d\n", clause_block->len);
    for (guint i = 0; i < clause_block->len; i++) {
        GArray *block = g_array_index(clause_block, GArray *, i);
//        printf("\tsub.len = %d\n", block->len);
        for (guint j = 0; j < block->len; j++) {
            if (j == 0) {
                printf("[");
                /* printf ("%d:[", i); */
            }

            if (j == block->len - 1) {
                printf("%d]\n", g_array_index(block, guint, j));
            }
            else {
                printf("%d, ", g_array_index(block, guint, j));
            }
        }
    }
    printf("\n");
}
