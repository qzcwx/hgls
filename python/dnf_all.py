#!/usr/bin/pypy

# compute DNF density for all files

import os
from dnf import DNF

# industrial instances
in_dir = "/home/chenwx/workspace/inst/sat14/crafted/SAT-dnf"
out_dir = "/home/chenwx/workspace/inst/sat14/crafted/SAT-dnf"

for f in os.listdir(in_dir):
    if f.endswith(".dnf"):
        in_path = "{}/{}".format(in_dir, f)
        out_path = "{}/{}.den".format(out_dir, f)
        dnf = DNF(in_path)
        print f
        with open(out_path, 'w') as out:
            dnf.compute_dnf_density()
            print dnf.compute_avg_density()
            for i in xrange(len(dnf.dnf_density)):
                den = dnf.dnf_density[i]
                rep = dnf.num_repetition[i]
                for j in xrange(rep):
                    print >> out, den