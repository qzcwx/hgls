#!/usr/bin/pypy

# DNF class for computing DNF density and performing analysis

import re
import argparse


class DNF:
    def __init__(self, fname=None):
        self.block_size = []
        self.num_repetition = []
        self.num_dnf = []

        self.dnf_density = []
        self.avg_density = None
        if fname:
            self.read_file(fname)

    def read_file(self, fname):
        with open(fname) as f:
            lines = f.readlines()
            stat_lines = [i for i in lines if i[0] == '*']
            self.block_size = [int(i[8]) for i in [re.split(",| ", line) for line in stat_lines]]
            self.num_repetition = [int(i[12]) for i in [re.split(",| ", line) for line in stat_lines]]
            self.num_dnf = [int(i[16]) for i in [re.split(",| ", line) for line in stat_lines]]
            # print self.block_size
            # print self.num_repetition
            # print self.num_dnf

    # compute DNF density, defined as $\frac{#dnf}{2^{block.size}}$
    def compute_dnf_density(self):
        if not self.num_dnf:
            return []
        if not self.dnf_density:
            self.dnf_density = [self.num_dnf[i] / float(2 ** self.block_size[i]) for i in range(len(self.num_dnf))]
        return self.dnf_density

    def compute_avg_density(self):
        if not self.num_dnf:
            return -1
        if not self.avg_density:
            if not self.dnf_density:
                self.compute_dnf_density()
            self.avg_density = sum(
                [self.dnf_density[i] * self.num_repetition[i] for i in range(len(self.num_dnf))]) / sum(
                self.num_repetition)
        return self.avg_density


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Process DNFs')
    parser.add_argument('dnf_path', type=str,
                        help='dnf input path.')
    parser.add_argument('out_path', type=str,
                        help="output path.")
    args = parser.parse_args()

    dnf = DNF(args.dnf_path)
    with open(args.out_path, 'w') as out:
        dnf.compute_dnf_density()
        for i in xrange(len(dnf.dnf_density)):
            den = dnf.dnf_density[i]
            rep = dnf.num_repetition[i]
            for j in xrange(rep):
                print >> out, den
