#!/usr/bin/python

# scripts for running experiment

import os
import subprocess as sp
import sys


# experiment on hyperplane initialization
def hyperInit():
    exe = './bin/Release/hgls'
    instRange = range(30)
    cRange = [-1, 0, 1, 2]
    vRange = [0, 1]
    wRange = [0, 1]
    # fRange = [0, 0.125, 0.25, 0.375, 0.5, 0.625, 0.75, 0.875, 1]
    fRange = [0.25]
    mRange = [215, 300, 400, 500]

    for m in mRange:
        # iBase = 'uf-n50-m215'
        iBase = 'uf-n50-m{}'.format(m)
        for inst in instRange:
            i = '{}-i{}.cnf'.format(iBase, inst)
            # s = '../result-allSol/ebf8e4d/{}.sol'.format(i)
            # s = '../MiniSAT-AllSol/res/{}.sol'.format(i)
            for c in cRange:
                if c < 3:
                    cmd = '{} -i {} -c {}'.format(exe, i, c)
                    print cmd
                    os.system(cmd)
                else:
                    # no complicated options
                    for f in fRange:
                        cmd = '{} -i {} -c {} -f {}'.format(exe, i, c, f)
                        print cmd
                        os.system(cmd)


# generate dnf with different thresholds to see what works best
def dnfWithDiffThres():
    import time
    inst = "/home/chenwx/workspace/inst/sat-indu-13-14-clause-sorted/app/MD5-28-1.cnf"
    out = "out.dnf"

    for i in xrange(1, 50):
        start = time.time()
        os.system("./hgls -i " + inst + " -t " + str(i) + " -d " + out)
        end = time.time()

        with open(out, 'r') as f:
            line = f.readline()
            terms = line.split()
            m = int(terms[len(terms) - 1])
            print "{}, {}, {:.3f}".format(i, m, end - start)


# generate commands for running DNF for all instances on multiple department machines
def genRunDNFJobs():
    instDir = "/s/chopin/m/proj/sched/chenwx/inst/sat14/crafted/SAT"
    instFile = "/s/chopin/m/proj/sched/chenwx/ldnf/py/2014.crafted.sat.desk"
    resDir = "/s/chopin/m/proj/sched/chenwx/inst/sat14/crafted/SAT-dnf"

    comFile = '/s/chopin/m/proj/sched/chenwx/hgls/python/commands.lst'
    binary = '/s/chopin/m/proj/sched/chenwx/hgls/cnf_to_dnf'

    with open(instFile, 'r') as f, open(comFile, 'w+') as c:
        for inst in f:
            name, _ = inst.split()
            fpath = "{}/{}.cnf".format(instDir, name)
            outfile = "{}/{}.dnf".format(resDir, name)

            if not os.path.exists(outfile):
                cmd = "{} -i {} -d {}".format(binary, fpath, outfile)
                print >> c, cmd
            else:
                print outfile, "exists"


# sequentially generate dnf
def gen_dnf():
    if len(sys.argv) == 1:
        print "Please specify instance directory"
        exit(1)

    dir_name = sys.argv[1]

    inst_name_range = ['rbcl_xits_14_SAT', 'rpoc_xits_15_SAT', "001-80-12",
                       "atco_enc3_opt1_03_53",
                       "atco_enc3_opt1_13_48",
                       "atco_enc3_opt1_04_50",
                       "atco_enc3_opt2_05_21",
                       "atco_enc3_opt2_18_44",
                       "vmpc-29", "grieu-vmpc-31", "vmpc-32", "vmpc-33", "stable-300-0.1-20-98765432130020"]

    binary = '/home/chenwx/workspace/HGLS/hgls'

    cnf_dir = "/home/chenwx/workspace/inst/{}/cnf".format(dir_name)
    cnf_lists = os.listdir(cnf_dir)

    dnf_dir = "/home/chenwx/workspace/inst/{}/dnf".format(dir_name)

    for cnf_file in cnf_lists:
        if cnf_file.endswith(".cnf"):
            cnf_path = "{}/{}".format(cnf_dir, cnf_file)
            inst_name = cnf_file.split(".cnf")[0]
            dnf_path = "{}/{}.dnf".format(dnf_dir, inst_name)
            if inst_name not in inst_name_range:
                print inst_name, "not in range"
                continue
            if os.path.isfile(dnf_path):
                print dnf_path, "exists"
            else:
                print inst_name
                p = sp.Popen([binary, "-i", cnf_path, "-d", dnf_path], stdout=sp.PIPE, stderr=sp.PIPE)
                out, err = p.communicate()
                print "out:", out
                print "err:", err
                print


genRunDNFJobs()
