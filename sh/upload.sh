#!/bin/bash
# upload source code and compile remotely for new set of experiments
lftp desktop << EOF
cd sched/
mirror -R -e ~/workspace/HGLS -x inst -x .git -X *.dnf* -X *.cnf*
bye
EOF

# compile
ssh eggs "cd /s/chopin/b/grad/chenwx/sched/HGLS;
cmake .
make clean
make -j 10
"
