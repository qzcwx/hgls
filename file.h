#ifndef _FILE_H
#define _FILE_H

#include <stdio.h>
#include <glib.h>
#include <stdlib.h>
#include <math.h>
#include <stdint.h>
#include <inttypes.h>

void readInstance(GArray **inst, guint *n, guint *m, char *fname);

void printInst(GArray *inst);

void parseInst(GArray *inst, guint **K, GArray ***mask, GArray ***negf, guint n, guint m, GArray ***lSub);

void prt(GArray *a);

void printParseInst(guint *k, GArray **mask, GArray **negf, guint n, guint m, GArray **lSub);

gint absComparator(int *item1, int *item2);

void clearInst(GArray **inst, guint **K, GArray ***mask, GArray ***negf, GArray ***lSub, guint n, guint m);

void export_dnf(GArray *dnf, guint n, char *fname);

void export_compact_dnf(GArray *dnf, GArray *dnf_clause_num, guint n, char *fname);

void export_compact_dnf_repetition(GArray *dnf, GArray *dnf_clause_num, guint n, gdouble time, gint threshold,
                                   char *fname);

void export_cnf_clauses(GArray *inst, GArray *cnf_clauses, char *fname);


/* helper utilities */
gint cstring_cmp(const void *a, const void *b);

/* free utilities */
void free_key_repetitions(gpointer key);

void free_value_repetitions(gpointer value);

#endif
