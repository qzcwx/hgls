#include "file.h"

gint
absComparator(int *item1, int *item2) {
    return abs(*item1) - abs(*item2);
}

void
readInstance(GArray **inst, guint *n, guint *m, char *fname) {
    /* open a file, and read its content into inst */
    FILE *fp;

    if ((fp = fopen(fname, "r")) == NULL) {
        fprintf(stderr, "Can't open input file %s\n", fname);
        exit(1);
    } else {
        size_t len = 0;
        char *s = NULL;
        char **sArr = NULL, **se;
        gint i;

        (*inst) = g_array_new(FALSE, FALSE, sizeof(GArray *));
        /* printf ("%d\n",*inst); */
        /* ins = g_array_new(FALSE, FALSE, sizeof(GArray*)); */
        /* g_array_new(FALSE, FALSE, sizeof(GArray*)); */

        while (getline(&s, &len, fp) != -1) {
            /* printf ("%s",s); */
            if (s[0] != 'c' && s[0] != 'p' && s[0] != '%') {
                /* allocate an array of  */
                GArray *line = g_array_new(FALSE, FALSE, sizeof(guint));
                sArr = g_strsplit_set(s, " \t", 0);
                for (se = sArr; *se; ++se) {
                    i = atoi(*se);
                    if (i) {
                        g_array_append_val(line, i);
                        /* printf ("%d\n",i); */
                    }
                }
                g_strfreev(sArr);
                g_array_sort(line, (GCompareFunc) absComparator);
                g_array_append_val((*inst), line);
                /* g_array_free(line,FALSE); */
            }
            else if (s[0] == 'p') {
                sArr = g_strsplit(s, " ", 0);
                gboolean isN = TRUE;
                for (se = sArr; *se; ++se) {
                    if (g_ascii_isdigit((*se)[0]) == TRUE) {
                        if (isN) {
                            /* printf ("*n %s\n",*se); */
                            *n = atoi(*se);
                            isN = FALSE;
                        }
                        else {
                            *m = atoi(*se);
                        }
                    }
                }
                g_strfreev(sArr);
            }
            else if (s[0] == '\%') {
                break;
            }
        }
        free(s);
        fclose(fp);
    }
}

void
printInst(GArray *inst) {
    /*  */
    printf("Num clause = %d\n", inst->len);
    /* print the content of the array of arrays representing instant*/
    for (gint i = 0; i < inst->len; ++i) {
        /* printf ("b index\n"); */
        GArray *ins = (GArray *) g_array_index(inst, GArray *, i);
        /* printf ("a index\n"); */
        printf("id = %d,  clause len = %d\n", i, ins->len);
        for (gint j = 0; j < ins->len; ++j) {
            printf("%d\t", g_array_index(ins, gint, j));
        }
        printf("\n");
    }
}

void
parseInst(GArray *inst, guint **K, GArray ***mask, GArray ***negf, guint n, guint m, GArray ***lSub) {
    /* parse instance stored in inst, and extract information needed for Walsh transform */
    gint I;
    guint bin, absVal;

    *K = g_new(guint, m);
    *mask = g_new(GArray *, m);
    *negf = g_new(GArray *, m);
    *lSub = g_new(GArray *, n);

    for (int i = 0; i < n; ++i) {
        (*lSub)[i] = NULL;
    }

    for (gint i = 0; i < inst->len; ++i) {
        GArray *ins = g_array_index(inst, GArray *, i);

        (*K)[i] = ins->len;
        (*mask)[i] = g_array_new(FALSE, FALSE, sizeof(guint));
        (*negf)[i] = g_array_new(FALSE, FALSE, sizeof(guint));

        for (gint j = 0; j < ins->len; ++j) {
            I = g_array_index(ins, guint, j);
            bin = (I > 0) ? 0 : 1;
            absVal = (guint) abs(I) - 1;
            if ((*lSub)[absVal] == NULL) {                   /* GArray not exist */
                (*lSub)[absVal] = g_array_new(FALSE, FALSE, sizeof(guint));
                g_array_append_val((*lSub)[absVal], i);
            }
            else {
                /* check existence of j */
                gboolean exist = FALSE;
                for (gint e = 0; e < (*lSub)[absVal]->len; e++) {
                    if (g_array_index((*lSub)[absVal], guint, e) == i) {
                        exist = TRUE;
                        break;
                    }
                }
                if (!exist) {
                    g_array_append_val((*lSub)[absVal], i);
                }
            }

            g_array_append_val((*negf)[i], bin);
            g_array_append_val((*mask)[i], absVal);
        }
    }
}

void
prt(GArray *a) {
    /* printf("Array holds: "); */
    int i;
    for (i = 0; i < a->len; i++)
        printf("%d ", g_array_index(a, int, i));
    printf("\n");
}

void
printParseInst(guint *k, GArray **mask, GArray **negf, guint n, guint m, GArray **lSub) {
    /* print the parsed instances */

    printf("n=%d, m=%d\n", n, m);

    for (guint i = 0; i < m; ++i) {
        printf("iSub=%d\n", i);
        printf("k=%d\n", k[i]);
        printf("mask\n");
        prt(mask[i]);
        printf("negf\n");
        prt(negf[i]);
    }

    for (int i = 0; i < n; ++i) {
        printf("var=%d\n", i);
        printf("lSub\n");
        prt(lSub[i]);
    }
}


void
clearInst(GArray **inst, guint **K, GArray ***mask, GArray ***negf, GArray ***lSub, guint n, guint m) {
    /* clear data structures created to hold instance file */
    g_free(*K);
    for (guint i = 0; i < m; ++i) {
        g_array_free((*mask)[i], TRUE);
        g_array_free((*negf)[i], TRUE);
    }

    for (guint i = 0; i < (*inst)->len; ++i) {
        g_array_free(g_array_index(*inst, GArray *, i), TRUE);
    }
    for (guint i = 0; i < n; i++) {
        if ((*lSub)[i])
            g_array_free((*lSub)[i], TRUE);
    }

    g_free(*mask);
    g_free(*negf);
    g_free(*lSub);
    g_array_free(*inst, TRUE);
}

void
printOptMiniSAT(GPtrArray *opt, guint n) {
    /* print optimal solution, one for each row */
    gboolean *sol;
    printf("opt-num = %d\n", opt->len);
    for (int i = 0; i < opt->len; ++i) {
        sol = g_ptr_array_index(opt, i);
        for (int j = 0; j < n; j++) {
            printf("%d", sol[j]);
        }
        printf("\n");
    }
}


void
export_dnf(GArray *dnf, guint n, char *fname) {
    /* export the dnf clauses to file */
    FILE *fp;
    if ((fp = fopen(fname, "w")) == NULL) {
        fprintf(stderr, "Can't open output file %s\n", fname);
        exit(1);
    }
    else {
        fprintf(fp, "p dnf %d %d\n", n, dnf->len);
        for (guint i = 0; i < dnf->len; ++i) {
            GArray *line = g_array_index(dnf, GArray *, i);
            for (guint j = 0; j < line->len; ++j) {
                fprintf(fp, "%d ", g_array_index(line, gint, j));
            }
            fprintf(fp, "0\n");
        }
        fclose(fp);
    }
}

void
export_compact_dnf(GArray *dnf, GArray *dnf_clause_num, guint n, char *fname) {
    /* export dnf using big integer compact representation */
    FILE *fp;
    if ((fp = fopen(fname, "w")) == NULL) {
        fprintf(stderr, "Can't open output file %s\n", fname);
        exit(1);
    } else {
        fprintf(fp, "p dnf %d %d\n", n, dnf->len);
        guint cid = 0;
        GArray *line;
        guint d;
        guint clause_num;
        uint64_t sum;
        guint one_sol;
        gint lit;
        uint64_t temp;
        /* printf("%lu, %ld\n", G_MAXULONG, G_MINLONG); */

        for (guint i = 0; i < dnf_clause_num->len; ++i) { /* go through all dnf clauses block by block  */
            sum = 0;
            clause_num = g_array_index(dnf_clause_num, guint, i);
            for (d = 0; d < clause_num; d++) {
                line = g_array_index(dnf, GArray *, cid);
                one_sol = 0;
                for (guint j = 0; j < line->len; ++j) {
                    lit = g_array_index(line, gint, j);
                    fprintf(fp, "%d ", lit);
                    if (lit > 0) {
                        /* the variable is assigned true */
                        temp = ((uint64_t) 1) << j;
                        one_sol += temp;
                    }
                }
                fprintf(fp, "0\n");
                fprintf(fp, "%d\n", one_sol);
                ++cid;
//                fprintf(fp, "before sum = %lu\n", sum);
                temp = ((uint64_t) 1) << one_sol;
                sum += temp;
//                fprintf(fp, "add = %lu\n", temp);
//                fprintf(fp, "after sum = %lu\n", sum);

            }
            fprintf(fp, "******* Big integer: %"PRIu64", Block size = %d\n", sum, line->len);
        }
        fclose(fp);
    }
}


void
export_compact_dnf_repetition(GArray *dnf, GArray *dnf_clause_num, guint n, gdouble time, gint threshold,
                              char *fname) {
    /* export dnf using big integer compact representation, group repeated patterns */
    FILE *fp;
    if ((fp = fopen(fname, "w")) == NULL) {
        fprintf(stderr, "Can't open output file %s\n", fname);
        exit(1);
    } else {
        fprintf(fp, "p dnf %d %d\n", n, dnf_clause_num->len);
        fprintf(fp, "time/threshold %f %d\n", time, threshold);
        guint cid = 0;
        guint d;
        GArray *line = NULL;
        guint clause_num;
        uint64_t sum;
        guint one_sol;
        gint lit;
        uint64_t temp;
        GHashTable *bigint_to_dnfclauseid = g_hash_table_new_full(g_str_hash, g_str_equal, free_key_repetitions,
                                                                  free_value_repetitions);
        // generates the initial hash table
        for (guint i = 0; i < dnf_clause_num->len; ++i) { /* go through all dnf clauses block by block  */
            clause_num = g_array_index(dnf_clause_num, guint, i);
            sum = 0;
            for (d = 0; d < clause_num; d++) {
                line = g_array_index(dnf, GArray *, cid);
                one_sol = 0;
                for (guint j = 0; j < line->len; ++j) {
                    lit = g_array_index(line, gint, j);
                    if (lit > 0) {
                        /* the variable is assigned true */
                        temp = ((uint64_t) 1) << j;
                        one_sol += temp;
                    }
                }
                ++cid;
                temp = ((uint64_t) 1) << one_sol;
                sum += temp;
            }

            /* Use (sum, line->len) as the key for dictionary */
            GString *indexStr = g_string_new("");
            g_string_append_printf(indexStr, "%"PRIu64"#%d", sum, line->len); /* mark */
            GArray *valPtr = g_hash_table_lookup(bigint_to_dnfclauseid, indexStr->str);
            if (valPtr) { // found previous entry
                g_array_append_val(valPtr, i);
            } else {
                GArray *dnf_clause_ids = g_array_new(FALSE, FALSE, sizeof(guint));
                g_array_append_val(dnf_clause_ids, i);
                g_hash_table_insert(bigint_to_dnfclauseid, g_strdup(indexStr->str), dnf_clause_ids);
            }
            g_string_free(indexStr, TRUE);
            // printf("******* Big integer: %lu, Block size = %d\n", sum, line->len);
        }

        // iterate through hash table to print dnf formula grouped by repetition
        GHashTableIter iter;
        gpointer key, value;
        g_hash_table_iter_init(&iter, bigint_to_dnfclauseid);
        guint hash_table_size = g_hash_table_size(bigint_to_dnfclauseid);
        char **hash_keys_array = (char **) g_hash_table_get_keys_as_array(bigint_to_dnfclauseid,
                                                                          &hash_table_size);
        qsort(hash_keys_array, hash_table_size, sizeof(char *), cstring_cmp);

        for (guint i = 0; i < hash_table_size; i++) {
            key = hash_keys_array[i];
            value = g_hash_table_lookup(bigint_to_dnfclauseid, key);
            GArray *dnf_block_ids = ((GArray *) value);
            gchar **tokens = g_strsplit(key, "#", -1);
            fprintf(fp, "******* Big integer: %"PRIu64", Block size = %d, Repetitions = %d",
                    g_ascii_strtoull(tokens[0], NULL, 10),
                    atoi(tokens[1]),
                    dnf_block_ids->len);
            g_strfreev(tokens);

            // export all DNFFs to file
            guint dnf_id = g_array_index(dnf_block_ids, guint, 0);
            fprintf(fp, ", #DNF = %d *******\n", g_array_index(dnf_clause_num, guint, dnf_id));

            for (guint k = 0; k < dnf_block_ids->len; k++) {
                dnf_id = g_array_index(dnf_block_ids, guint, k);
//                printf("dnf_id = %d\n", dnf_id);
                guint line_idx = 0;
                for (guint j = 0; j < dnf_id; j++) {
                    line_idx += g_array_index(dnf_clause_num, guint, j);
                }
                line = g_array_index(dnf, GArray *, line_idx);
                for (guint j = 0; j < line->len; ++j) {
                    lit = g_array_index(line, gint, j);
                    fprintf(fp, "%d ", abs(lit));
                }
                fprintf(fp, "\n");
            }
        }

        free(hash_keys_array);
        g_hash_table_destroy(bigint_to_dnfclauseid);
    }
    fclose(fp);
}

void
export_cnf_clauses(GArray *inst, GArray *cnf_clauses, char *fname) {
    FILE *fp;
    if ((fp = fopen(fname, "a")) == NULL) {
        fprintf(stderr, "Can't open output file %s to append CNF clauses\n", fname);
        exit(1);
    } else {
        fprintf(fp, "******* CNF clauses *******\n");
        GArray *clause;
        guint cid;
        gint lit;
        for (guint i = 0; i < cnf_clauses->len; i++) { // evaluate all clauses in the block under current assignment
            cid = g_array_index(cnf_clauses, guint, i);
            clause = g_array_index(inst, GArray *, cid);
            for (guint j = 0; j < clause->len; j++) {
                lit = g_array_index(clause, gint, j);
                fprintf(fp, "%d ", lit);
            }
            fprintf(fp, "0\n");
        }
    }
    fclose(fp);
}

/* Helper utility */

/* compare big int hash table (block size, integer) */
gint cstring_cmp(const void *a, const void *b) {
    const char **a_ptr = (const char **) a;
    const char **b_ptr = (const char **) b;
    gchar **a_tokens = g_strsplit(*a_ptr, "#", -1);
    gchar **b_tokens = g_strsplit(*b_ptr, "#", -1);
    guint a_block_size = (guint) atoi(a_tokens[1]);
    guint b_block_size = (guint) atoi(b_tokens[1]);
    int res;
//    printf("%s\n", *a_ptr);
//    printf("%s\n", *b_ptr);
    if (a_block_size != b_block_size) {
        res = a_block_size > b_block_size ? 1 : -1;
//        printf("block_size res = %d\n", res);
    } else {
        res = g_ascii_strtoull(a_tokens[0], NULL, 10) > g_ascii_strtoull(b_tokens[0], NULL, 10) ? 1 : -1;
//        printf("big_int res = %d\n", res);
    }
    g_strfreev(a_tokens);
    g_strfreev(b_tokens);
    return res > 0 ? 1 : -1;
}


/* free utilities */
void
free_key_repetitions(gpointer key) {
    g_free((char *) key);
}


void
free_value_repetitions(gpointer value) {
    g_array_free((GArray *) value, TRUE);

}

