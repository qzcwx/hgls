CC      =       gcc
CFLAGS = -Wall -O3 $(shell pkg-config --cflags glib-2.0) -std=gnu99 -g
LIBS    =       -lm -lglib-2.0 $(shell pkg-config --cflags glib-2.0)
OBJS    =       main.o file.o block.o

cnf_to_dnf    :        $(OBJS)
	$(CC) $(CFLAGS) -o cnf_to_dnf $(OBJS) $(LIBS)

main.o  :       main.c
	$(CC) $(CFLAGS)  main.c -c 

file.o	: file.c file.h
	$(CC) $(CFLAGS) file.c -c

block.o	: block.c block.h
		$(CC) $(CFLAGS) block.c -c

clean   :
	rm  cnf_to_dnf *.o
